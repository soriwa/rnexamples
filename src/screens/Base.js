/**
 * React 기초
 */
import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, TextInput } from 'react-native';

class SearchBar extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    text: PropTypes.string.isRequired,
    isEmpty: PropTypes.bool,
    size: PropTypes.number,
  }

  static defaultProps = {
    text: '하하하',
  }

  constructor(props) {
    super(props);
    // this.state = {
    //   text: '',
    // };
  }

  render() {
    return (
      <View style={searchStyles.container}>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={(text) => this.setState({text})}
          // value={this.state.text}
          value={this.props.text}
        />
      </View>
    );
  }
}

// SearchBar.propTypes = {
//   children: PropTypes.node,
//   text: PropTypes.string.isRequired,
//   isEmpty: PropTypes.bool,
//   size: PropTypes.number,
// };

// SearchBar.defaultProps = {
//   text: 'hahaha',
// };

const searchStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    height: 60,
    backgroundColor: 'yellow',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
});

export default class Base extends React.Component {
  static navigationOptions = {
    title: 'Base',
  };

  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };
  }

  render() {
    return (
      <View style={s.container}>
        <SearchBar />
      </View>
    );
  }
}

const s = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
});


