import React from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator } from 'react-native';
import axios from 'axios';

export default class List extends React.Component {
  static navigationOptions = {
    title: 'Fetch',
  };

  constructor(props){
    super(props);
    this.state ={ isLoading: true }

    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();
  }

  componentDidMount() {
    axios.get('https://facebook.github.io/react-native/movies.json', { cancelToken: this.source.token })
      .then((responseJson) => {
        console.log(responseJson)
        this.setState({
          isLoading: false,
          dataSource: responseJson.data.movies,
        }, function(){

        });
      })
      .catch((error) => {
        console.error(error);
      });

    // fetch('https://facebook.github.io/react-native/movies.json')
    //   .then((response) => response.json())
    //   .then((responseJson) => {
    //     this.setState({
    //       isLoading: false,
    //       dataSource: responseJson.movies,
    //     }, function(){

    //     });
    //   })
    //   .catch((error) =>{
    //     console.error(error);
    //   });

    // try {
    //   const res = await fetch('https://facebook.github.io/react-native/movies.json');
    //   const rs = await res.json();
    //   console.log('rs:', rs);
    //   this.setState({
    //     isLoading: false,
    //     dataSource: rs.movies,
    //   }, function(){

    //   });
    // } catch (err) {
    //   console.error(err);
    // }

  }

  componentWillUnmount() {
    this.source.cancel('cancel');
  }

  render(){
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      <View style={{flex: 1, paddingTop:20}}>
        <FlatList
          data={this.state.dataSource}
          renderItem={({item}) => <Text>{item.title}, {item.releaseYear}</Text>}
          keyExtractor={(item, index) => item.title}
        />
      </View>
    );
  }
}

/*

https://facebook.github.io/react-native/movies.json - response

{
  "title": "The Basics - Networking",
  "description": "Your app fetched this from a remote endpoint!",
  "movies": [
    { "title": "Star Wars", "releaseYear": "1977"},
    { "title": "Back to the Future", "releaseYear": "1985"},
    { "title": "The Matrix", "releaseYear": "1999"},
    { "title": "Inception", "releaseYear": "2010"},
    { "title": "Interstellar", "releaseYear": "2014"}
  ]
}

*/