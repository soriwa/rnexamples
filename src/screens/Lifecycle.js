/**
 * React 생명주기 예제 
 */
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Lifecycle extends React.Component {
  static navigationOptions = {
    title: 'Life cycle',
  };

  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };

    this._timer = setInterval(() => {
      this.setState(prevState => {
        return { count: prevState.count + 1 };
      });
    }, 3000);
  }

  componentWillMount() {
    // console.log('componentWillMount:' + this.state.count);
    console.log(`[1] componentWillMount: ${this.state.count}`);
  }

  componentDidMount() {
    console.log(`[2] componentDidMount: ${this.state.count}`);
  }

  componentWillReceiveProps (nextProsp) {
    console.log(`[3] componentWillReceiveProps: ${this.state.count}`);
  }

  shouldComponentUpdate (nextProsp, nextState) {
    console.log(`[4] shouldComponentUpdate: ${this.state.count}`);
    // return true;
    return this.state.count < 3;
  }

  componentWillUpdate (nextProsp, nextState) {
    console.log(`[5] componentWillUpdate: ${this.state.count}`);
  }

  componentDidUpdate (prevProsp, prevState) {
    console.log(`[6] componentDidUpdate: ${this.state.count}`);
  }

  componentWillUnmount() {
    clearInterval(this._timer);
    console.log(`[7] componentWillUnmount: ${this.state.count}`);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>count: {this.state.count}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
