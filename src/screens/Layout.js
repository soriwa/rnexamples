import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { StatusBar, Platform } from 'react-native';

export default class Layout extends React.Component {
  static navigationOptions = {
    title: 'Layout 1',
    // header: null,
    // headerStyle: {
    //   backgroundColor: '#3A60A8',
    // },
    // headerTintColor: '#F9FFFF',
    // headerTitleStyle: {
    //   fontWeight: 'bold',
    // },
  };

  render() {
    return (
      <View style={s.container}>
        <View style={s.left}></View>
        <View style={s.right}>
          <View style={s.rightTop}></View>
          <View style={s.rightBottom}></View>
        </View>
      </View>
    );
  }
}

const s = StyleSheet.create({
  statusbar: {
    height: 20,
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  left: {
    flex: 1,
    backgroundColor: 'yellow',
  },
  right: {
    flex: 2,
    flexDirection: 'column',
    backgroundColor: 'green',
  },
  rightTop: {
    flex: 2,
    backgroundColor: 'red',
  },
  rightBottom: {
    flex: 8,
    backgroundColor: 'blue',
  },
});
