import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

class Item extends React.Component {
  render() {
    return (
      <View style={{ 
        height: 80, borderBottomWidth: StyleSheet.hairlineWidth, backgroundColor: 'yellow',
        flexDirection: 'row',
      }}>
        <View>
          <View style={{ backgroundColor: 'orange', width: 50, height: 50, borderRadius: 25 }}></View>
        </View>
        <View>
          <Text>텍스트1</Text>
          <Text>텍스트2</Text>
          <Text>텍스트3</Text>
        </View>
      </View>
    );
  }
}

export default class Layout3 extends React.Component {
  static navigationOptions = {
    title: 'Layout 3',
  };

  renderCircle = () => {
    return <View style={{ backgroundColor: 'orange', width: 50, height: 50, borderRadius: 25 }}></View>;
  };

  render() {
    return (
      <View style={s.container}>
        <View style={s.header}>
          <Text>Header</Text>
        </View>
        <View style={s.body}>
          <Item/>
        </View>
        <View style={s.footer}>
          <Text>Footer</Text>
        </View>
      </View>
    );
  }
}

const s = StyleSheet.create({
  container: {
    flex: 1,
    // flexDirection: 'row',
    backgroundColor: '#fff',
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  header: {
    // flex: 1,
    backgroundColor: 'yellow',
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  body: {
    flex: 1,
    backgroundColor: 'green',
    /**
     * justifyContent 규칙 
     * space-around: 아이템 간의 간격은 균등분할, 첫 아이템과 마지막 아이템의 각 끝은 균등분할된 간격의 절반
     * space-between: 아이템 간의 간격은 균등분할, 첫 아이템과 마지막 아이템의 각 끝은 간격없음
     * space-evenly: 모든 아이템 간의 간격은 균등분할 
     */
    justifyContent: 'center', //  flex-start, center, flex-end, space-around, space-between, space-evenly
    /**
     * alignItems 규칙
     * stretch: 아이템의 교차축 방향의 크기가 꽉 채워지도록 늘어남 - 교차축 방향의 크기 속성이 설정되어 있지 않아야 적용됨 
     *          e.g. flexDirection: row 일 때 교차축은 column 이므로 height 속성이 없어야 stretch 효과가 나타남 
     */
    alignItems: 'stretch', //  flex-start, center, flex-end, stretch
  },
  footer: {
    // flex: 1,
    backgroundColor: 'red',
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
