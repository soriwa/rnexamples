import React from 'react';
import { StyleSheet, View, Text, FlatList, ActivityIndicator } from "react-native";
import { ListItem, SearchBar } from "react-native-elements";
import axios from 'axios';

export default class UserList extends React.Component {
  static navigationOptions = {
    title: 'User List',
  };

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false
    };

    const CancelToken = axios.CancelToken;
    this.source = CancelToken.source();
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  componentWillUnmount() {
    this.source.cancel('cancel');
  }

  makeRemoteRequest = () => {
    const { page, seed } = this.state;
    const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    this.setState({ loading: true });

    axios.get(url, { cancelToken: this.source.token })
      .then((res) => {
        // console.log(res);
        this.setState({
          data: page === 1 ? res.data.results : [...this.state.data, ...res.data.results],
          error: res.error || null,
          loading: false,
          refreshing: false
        }, () => {});
      })
      .catch((error) => {
        console.error(error);
      });

    // fetch(url)
    //   .then(res => res.json())
    //   .then(res => {
    //     // console.log('rs:', res.results);
    //     this.setState({
    //       data: page === 1 ? res.results : [...this.state.data, ...res.results],
    //       error: res.error || null,
    //       loading: false,
    //       refreshing: false
    //     });
    //   })
    //   .catch(error => {
    //     this.setState({ error, loading: false });
    //   });
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        seed: this.state.seed + 1,
        refreshing: true
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };

  renderHeader = () => {
    return <SearchBar placeholder="Type Here..." lightTheme round />;
  };

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    return (
      <FlatList
        style={{ flex: 1 }}
        data={this.state.data}
        renderItem={({ item }) => (
          <ListItem
            title={`${item.name.first} ${item.name.last}`}
            subtitle={item.email}
            leftAvatar={{ rounded: true, source: { uri: item.picture.thumbnail } }}
            containerStyle={{ borderBottomWidth: 0 }}
          />
        )}
        keyExtractor={item => item.email}
        ItemSeparatorComponent={this.renderSeparator}
        ListHeaderComponent={this.renderHeader}
        ListFooterComponent={this.renderFooter}
        onRefresh={this.handleRefresh}
        refreshing={this.state.refreshing}
        onEndReached={this.handleLoadMore}
        onEndReachedThreshold={0.9}
      />
    );
  }

}
