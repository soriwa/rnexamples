import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

export default class Main extends React.Component {
  static navigationOptions = {
    title: 'Main',
    header: null,
  };

  /*
  // 생성자에서 this 바인딩 
  constructor(props) {
    super(props);
    this._goLifecycle = this._goLifecycle.bind(this);
    this._goBase = this._goBase.bind(this);
  }

  _goLifecycle() {
    this.props.navigation.navigate('Lifecycle');
  }
  _goBase() {
    this.props.navigation.navigate('Base');
  }
  */

  // Class Properties를 이용한 자동 this 바인딩 - 아직 표준이 아님 
  _goLifecycle = () => this.props.navigation.navigate('Lifecycle');
  _goBase = () => this.props.navigation.navigate('Base');
  _goLayout1 = () => this.props.navigation.navigate('Layout1');
  _goLayout2 = () => this.props.navigation.navigate('Layout2');
  _goLayout3 = () => this.props.navigation.navigate('Layout3');
  _goScroll = () => this.props.navigation.navigate('Scroll');
  _goList = () => this.props.navigation.navigate('List');
  _goScreen = (target) => this.props.navigation.navigate(target);
  // _goUserList = () => this.props.navigation.navigate('UserList');

  render() {
    return (
      <View style={s.container}>
        <Button onPress={this._goLifecycle} title="Life Cycle"/>
        <Button onPress={this._goBase} title="Base"/>
        <Button onPress={this._goLayout1} title="Layout1"/>
        <Button onPress={this._goLayout2} title="Layout2"/>
        <Button onPress={this._goLayout3} title="Layout3"/>
        <Button onPress={this._goScroll} title="ScrollView"/>
        <Button onPress={this._goList} title="List"/>
        <Button onPress={() => this._goScreen('Fetch')} title="Fetch"/>
        <Button onPress={() => this._goScreen('UserList')} title="User List"/>
        {/* <Button onPress={this._goUserList} title="User List"/> */}
      </View>
    );
  }
}

const s = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#fff',
    justifyContent: 'space-evenly', //  space-around, space-between, space-evenly
    alignItems: 'stretch',
  },
  button: {
    marginBottom: 10,
  }
});
