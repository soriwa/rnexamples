import React from 'react';
import { StyleSheet, Text, View, FlatList, SectionList } from 'react-native';

const flatData = [
  {key: 'Alex'},
  {key: 'Allen'},
  {key: 'Allie'},
  {key: 'Amanda'},
  {key: 'Anna'},
  {key: 'Annie'},
  {key: 'Alice'},
  {key: 'Aimee'},
  {key: 'Devin'},
  {key: 'David'},
  {key: 'Demian'},
  {key: 'Debora'},
  {key: 'Daisy'},
  {key: 'Dolly'},
  {key: 'Debian'},
  {key: 'Jackson'},
  {key: 'James'},
  {key: 'Joel'},
  {key: 'John'},
  {key: 'Jillian'},
  {key: 'Jimmy'},
  {key: 'Julie'},
  {key: 'Jupiter'},
  {key: 'Jacky'},
];

const sectionData = [
  {title: 'A', data: ['Alex', 'Allen', 'Allie', 'Amanda', 'Anna', 'Annie', 'Alice', 'Aimee']},
  {title: 'D', data: ['Devin', 'David', 'Demian', 'Debora', 'Daisy', 'Dolly', 'Debian']},
  {title: 'J', data: ['Jackson', 'James', 'Jillian', 'Jimmy', 'Joel', 'John', 'Julie', 'Jupiter', 'Jacky']},
];

export default class List extends React.Component {
  static navigationOptions = {
    title: 'List',
  };

  _renderFlatList = () => (
    <FlatList
      data={flatData}
      renderItem={({item}) => <Text style={s.item}>{item.key}</Text>}
    />
  );

  _renderSectionList = () => (
    <SectionList
      sections={sectionData}
      renderItem={({item}) => <Text style={s.item}>{item}</Text>}
      renderSectionHeader={({section}) => <Text style={s.sectionHeader}>{section.title}</Text>}
      keyExtractor={(item, index) => index}
      style={s.sectionList}
    />
  );

  render() {
    return (
      <View style={s.container}>
        {/* {this._renderFlatList()} */}
        {this._renderSectionList()}
      </View>
    );
  }
}

const s = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  sectionList: {
    // flex: 1,
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },
});
