import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';

const IMG = 'https://randomuser.me/api/portraits/thumb/men/83.jpg';

export default class Scroll extends React.Component {
  static navigationOptions = {
    title: 'ScrollView',
  };

  render() {
    return (
      <ScrollView 
        // horizontal
      >
        <Text style={{fontSize:96}}>Scroll me plz</Text>
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Text style={{fontSize:96}}>If you like</Text>
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Text style={{fontSize:96}}>Scrolling down</Text>
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Text style={{fontSize:96}}>What's the best</Text>
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Text style={{fontSize:96}}>Framework around?</Text>
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Image source={{uri: IMG}} style={{width: 48, height: 48}} />
        <Text style={{fontSize:80}}>React Native</Text>
      </ScrollView>
    );
  }
}

const s = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
