import React from 'react';
import { Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Main from './src/screens/Main';
import Lifecycle from './src/screens/Lifecycle';
import Base from './src/screens/Base';
import Layout1 from './src/screens/Layout';
import Layout2 from './src/screens/Layout2';
import Layout3 from './src/screens/Layout3';
import Scroll from './src/screens/Scroll';
import List from './src/screens/List';
import Fetch from './src/screens/Fetch';
import UserList from './src/screens/UserList';
import Hello from './src/screens/Hello';

export default createStackNavigator({
  Main: {
    screen: Main
  },
  Lifecycle: {
    screen: Lifecycle
  },
  Base: {
    screen: Base
  },
  Layout1: {
    screen: Layout1
  },
  Layout2: {
    screen: Layout2
  },
  Layout3: {
    screen: Layout3
  },
  Scroll: {
    screen: Scroll
  },
  List: {
    screen: List
  },
  Fetch: {
    screen: Fetch
  },
  UserList: {
    screen: UserList
  },
  Hello: {
    screen: Hello
  },
});